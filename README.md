# 🟰 spaCy Similarity Demo

> Demo of Semantic Similarity using word vector or transformer models

Realized in _Python_ using mainly [`spaCy`](https://spacy.io) in a [`Streamlit`](https://www.streamlit.io) app.

## Installation

```sh
poetry install

# Activate environment
poetry shell
```

### English NLP Models

```sh
python -m spacy download en_core_web_md
python -m spacy download en_core_web_lg
python -m spacy download en_core_web_trf

python -m spacy validate
```

### German NLP Models

```sh
python -m spacy download de_core_news_lg
python -m spacy download de_dep_news_trf

python -m spacy validate
```

## Usage

```sh
poetry shell
streamlit run spacy_similarity_demo.py
```

The demo will open in the browser under <http://localhost:8501>.

After selecting the model language and language models, the user has the choice
between three modes to compare the selected models:

1. **1-on-1**: Determine similarity between two words or phrases for all selected models
2. **Multi-Ranking**: Search for the most similar words or phrases inputted into the text area in comparison to the query for all selected models
3. **Similarity-Matrix**: See a colored similarity matrix between all words or phrases inputted into the text area for all selected models
