from pathlib import Path
from typing import Dict, List, Text

import pandas
import seaborn
from sklearn.metrics.pairwise import cosine_similarity
import spacy
from spacy.language import Language
from spacy.tokens import Doc
import streamlit

EN_SPACY_MODELS = [
    "en_core_web_md",
    "en_core_web_lg",
    "en_core_web_trf",
]

DE_SPACY_MODELS = [
    "de_core_news_lg",
    "de_dep_news_trf",
]

SPACY_MODELS = {
    "en": EN_SPACY_MODELS,
    "de": DE_SPACY_MODELS,
}

streamlit.set_page_config(
    page_title="spaCy Similarity Demo",
    page_icon="🟰"
)

streamlit.title("🟰 spaCy Similarity Demo")

SHOW_README: bool = streamlit.checkbox("Show README")

if SHOW_README:
    streamlit.write("---")
    streamlit.write(Path("./README.md").read_text())
    streamlit.write("---")

streamlit.header("Language and Model Selection")

LANGUAGE: Text = streamlit.selectbox(
    "Language Choice",
    list(SPACY_MODELS.keys()),
)

LANGUAGE_SPACY_MODELS: List[Text] = SPACY_MODELS[LANGUAGE]

streamlit.subheader("Model Selection")

SELECTED_SPACY_MODELS = {
    model_name: streamlit.checkbox(
        model_name,
    )
    for model_name in LANGUAGE_SPACY_MODELS
}

streamlit.write("The following language models are selected for the demo:")

streamlit.write(
    [model for model, selected in SELECTED_SPACY_MODELS.items() if selected]
)


@streamlit.cache(allow_output_mutation=True)
def initialize_nlp_models(selected_models: Dict[Text, bool]) -> Dict:
    return {
        model_name: spacy.load(model_name)
        for model_name, selected in selected_models.items()
        if selected
    }


def pairwise_similarity(first: Doc, second: Doc) -> float:
    similarity: float = first.similarity(second)

    if not similarity:
        similarity = float(
            cosine_similarity(
                first._.trf_data.tensors[1],
                second._.trf_data.tensors[1],
            )[0][0]
        )

    return round(similarity, ndigits=4)


SELECTED_NLP_MODELS: Dict[Text, Language] = initialize_nlp_models(
    SELECTED_SPACY_MODELS,
)

streamlit.header("Demo Mode Selection")

MODE: Text = streamlit.radio(
    "Mode",
    ("1-on-1", "Multi-Ranking", "Similarity-Matrix"),
)

if MODE in ("Multi-Ranking", "Similarity-Matrix"):
    DATASET_RAW: Text = streamlit.text_area(
        "Text parts separated by line breaks",
        height=200,
    )
    DATASET: List[Text] = str(DATASET_RAW).splitlines(keepends=False)

if MODE == "1-on-1":

    streamlit.write(
        "In this mode you can compare the similarity "
        "between two words or phrases over multiple models."
    )

    first, second = streamlit.text_input("First"), streamlit.text_input("Second")

    if first and second:
        for name, nlp_model in SELECTED_NLP_MODELS.items():
            streamlit.subheader(f"`{name}`")
            first_doc, second_doc = nlp_model(first), nlp_model(second)
            streamlit.write(pairwise_similarity(first_doc, second_doc))

elif MODE == "Multi-Ranking":

    streamlit.write(
        "In this mode you can query a dataset of lines "
        "according to similarity over multiple models."
    )

    QUERY: Text = streamlit.text_input("Query")

    if DATASET and QUERY:
        for name, nlp_model in SELECTED_NLP_MODELS.items():
            streamlit.subheader(f"`{name}`")

            query_doc: Doc = nlp_model(QUERY)

            ranking = sorted(
                [
                    (
                        line,
                        pairwise_similarity(nlp_model(QUERY), nlp_model(line)),
                    )
                    for line in DATASET
                ],
                key=lambda item: item[1],
                reverse=True,
            )

            for line, pairwise_similarity_value in ranking:
                streamlit.write(f"`{pairwise_similarity_value}`: '{line}'")

elif MODE == "Similarity-Matrix":

    streamlit.write(
        "In this mode you can create a dataset of lines "
        "and see a similarity matrix of all entries."
    )

    if DATASET:
        for name, nlp_model in SELECTED_NLP_MODELS.items():
            streamlit.subheader(f"`{name}`")

            SIMILARITY_MATRIX = pandas.DataFrame(
                [
                    [
                        pairwise_similarity(nlp_model(first), nlp_model(second))
                        for first in DATASET
                    ]
                    for second in DATASET
                ],
                index=DATASET,
                columns=DATASET,
            )

            SIMILARITY_MATRIX = SIMILARITY_MATRIX.style.background_gradient(
                cmap=seaborn.light_palette("Green", as_cmap=True), vmin=0.0, vmax=1.0
            )

            streamlit.table(SIMILARITY_MATRIX)
